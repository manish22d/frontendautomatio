package com.frontEndAutomation.TestCases;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;

import java.io.IOException;
import java.lang.reflect.Method;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.frontendAutomation.BaseClass.TestBase;
import com.frontendAutomation.Pages.AddCustomerPage;
import com.frontendAutomation.Pages.CustomersPage;
import com.frontendAutomation.Pages.DashboardPage;
import com.frontendAutomation.Pages.HomePage;
import com.frontendAutomation.Pages.SchedulePage;
import com.frontendAutomation.Pages.StudioPage;
import com.frontendAutomation.Utilities.TestUtility;
import com.relevantcodes.extentreports.LogStatus;

public class ScheduleClassesTest extends TestBase {

	StudioPage studioPage;
	HomePage homePage;
	DashboardPage dashboardPage;
	CustomersPage customersPage;
	AddCustomerPage addCustomerPage;
	SchedulePage schedulePage;
	TestUtility testUtil;

	public ScheduleClassesTest() {
		super();
	}

	@BeforeTest(alwaysRun = true)
	public void setUp() {
		System.out.println(System.getProperty("os.name"));
		initialization();
		Log.info("Application Launched Successfully");

		testUtil = new TestUtility();
		studioPage = new StudioPage();
		dashboardPage = studioPage.login(property.getProperty("Username"), property.getProperty("Password"));
	}

	@Test(priority = 1, enabled = false)
	public void verifyAdminIsAbleToAddCustomer(Method method) throws IOException {
		extentTest = extent.startTest(method.getName());
		String loginPageTitle = driver.getTitle();
		System.out.println("The Login Page Title is ::: " + loginPageTitle);
		Assert.assertEquals(loginPageTitle, "Welcome to KRIYA Online Booking System");

		customersPage = dashboardPage.navigateToCustomerPage();
		addCustomerPage = customersPage.navigateAddCustomerPage();
		customersPage = addCustomerPage.addANewCustomer("Manish", "Kumar", "abc@xyz.com");
		String actualSuccessMsg = customersPage.getSuccessMsg();
		Assert.assertEquals(actualSuccessMsg, "Customer added successfully");
		extentTest.log(LogStatus.INFO, extentTest.addScreenCapture(TestUtility.takeScreenshotAtEndOfTest().getPath()));
		Log.info("Login Page Title Verified");
	}

	// Verify atleast one class is available
	@Test(priority = 1, enabled = true)
	public void verifyNumberOfClassesInPage(Method method) {
		extentTest = extent.startTest(method.getName());
		schedulePage = dashboardPage.navigateToSchedulePage();
		assertThat(schedulePage.getNumberOfClasses(), is(greaterThan(0)));
	}

	// Verify Open, Edit and Delete Class is available in each class
	@Test(priority = 2, enabled = true)
	public void verifyClassesInfoDispalyedProperly(Method method) throws InterruptedException {
		extentTest = extent.startTest(method.getName());
		schedulePage = dashboardPage.navigateToSchedulePage();
		schedulePage.verifyButtonDisplayedInClass();
	}

	// Verify we are able to add customer having visits remaining
	@Test(priority = 3, enabled = true)
	public void verifyAddCustomerWithRemainingVisitWithOneOffPkg(Method method) throws InterruptedException {
		extentTest = extent.startTest(method.getName());
		schedulePage = dashboardPage.navigateToSchedulePage();
		schedulePage.selectClassAfterCurrentDate();
		schedulePage.clickOnOpenClass();
		schedulePage.addWithoutPackage("Ash Blahh (ash2@gmail.com)");
		assertThat("Name did not matched in table",
				schedulePage.verifyCustomerGotAddedInList("Ash Blahh (ash2@gmail.com)"));
	}

	// Verify we are able to add customer with recurring package
	@Test(priority = 3, enabled = true)
	public void verifyAddCustomerWithRecurringPkg(Method method) throws InterruptedException {
		extentTest = extent.startTest(method.getName());
		schedulePage = dashboardPage.navigateToSchedulePage();
		schedulePage.selectClassAfterCurrentDate();
		schedulePage.clickOnOpenClass();
		schedulePage.selectCustomer("Ash Dee (kriyadev5@gmail.com)");
		assertThat("Name did not matched in table",
				schedulePage.verifyCustomerGotAddedInList("Ash Dee (kriyadev5@gmail.com)"));
	}

	// Verify we are able to add customer without Pass
	@Test(priority = 4, enabled = true)
	public void verifyAddCustomerWithOutPass(Method method) throws InterruptedException {
		extentTest = extent.startTest(method.getName());
		schedulePage = dashboardPage.navigateToSchedulePage();
		schedulePage.selectClassAfterCurrentDate();
		schedulePage.clickOnOpenClass();
		schedulePage.addWithoutPackage("Manish Kumar (abc@xyz.com)");
		assertThat("Name did not matched in table",
				schedulePage.verifyCustomerGotAddedInList("Manish Kumar (abc@xyz.com)"));
	}

	// Verify we are able to add customer after adding package
	@Test(priority = 5, enabled = false)
	public void verifyAddCustomerWithPackage(Method method) throws InterruptedException {
		extentTest = extent.startTest(method.getName());
		schedulePage = dashboardPage.navigateToSchedulePage();
		schedulePage.selectClassAfterCurrentDate();
		schedulePage.clickOnOpenClass();
		schedulePage.addWithPackage("Automation UnlimitedVisit (automation@test.com)");
		assertThat("Name did not matched in table",
				schedulePage.verifyCustomerGotAddedInList("Automation UnlimitedVisit (automation@test.com)"));
	}

	@DataProvider(name = "SearchProvider")
	public String[][] getDataFromDataprovider() {
		return TestUtility.getTestData("Contacts");
	}

}

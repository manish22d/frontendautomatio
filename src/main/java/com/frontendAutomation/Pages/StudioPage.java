package com.frontendAutomation.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.frontendAutomation.BaseClass.TestBase;

public class StudioPage extends TestBase {

	@FindBy(name = "username")
	WebElement userName;

	@FindBy(name = "password")
	WebElement password;

	@FindBy(xpath = "//*[contains(text(),'Sign in')]")
	WebElement submitBtn;

	public StudioPage() {
		PageFactory.initElements(driver, this);
	}

	public DashboardPage login(String id, String pwd) {
		System.out.println(id + " " + pwd);
		userName.sendKeys(id);
		password.sendKeys(pwd);
		submitBtn.click();

		return new DashboardPage();
	}
}

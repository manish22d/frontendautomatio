package com.frontendAutomation.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.frontendAutomation.BaseClass.TestBase;

public class AddCustomerPage extends TestBase {

	@FindBy(name = "first_name")
	WebElement fName;

	@FindBy(name = "last_name")
	WebElement lName;

	@FindBy(name = "email")
	WebElement email;

	@FindBy(id = "submit")
	WebElement submitBtn;

	public AddCustomerPage() {
		PageFactory.initElements(driver, this);
	}

	public CustomersPage addANewCustomer(String fristName, String lastName, String Email) {
		fName.sendKeys(fristName);
		lName.sendKeys(lastName);
		email.sendKeys(Email);

		submitBtn.click();
		
		return new CustomersPage();
	}
}

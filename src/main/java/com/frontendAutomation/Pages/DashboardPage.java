package com.frontendAutomation.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.frontendAutomation.BaseClass.TestBase;

public class DashboardPage extends TestBase {

	@FindBy(xpath = "//ul[@class='menu-items scroll-content']/li[1]")
	WebElement schedule;

	@FindBy(xpath = "//ul[@class='menu-items scroll-content']/li[2]")
	WebElement customers;

	@FindBy(xpath = "//ul[@class='menu-items scroll-content']/li[3]")
	WebElement reports;

	@FindBy(xpath = "//ul[@class='menu-items scroll-content']/li[4]")
	WebElement settings;

	public DashboardPage() {
		PageFactory.initElements(driver, this);
	}

	public SchedulePage navigateToSchedulePage() {
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(schedule));
		schedule.click();
		
		return new SchedulePage();
	}

	public CustomersPage navigateToCustomerPage() {
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(customers));
		customers.click();
		
		return new CustomersPage();
	}

	public void navigateToReportPage() {
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(reports));
		reports.click();
	}

	public void navigateToSettingPage() {
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(settings));
		settings.click();
	}
}

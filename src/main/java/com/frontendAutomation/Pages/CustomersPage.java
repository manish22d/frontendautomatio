package com.frontendAutomation.Pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.frontendAutomation.BaseClass.TestBase;

public class CustomersPage  extends TestBase {
	
	@FindBy(xpath = "//*[@class='pull-right']/a/i")
	WebElement addCustomer;
	
	@FindBy(xpath = "//*[@class='alert alert-success']")
	WebElement successBanner;
	
	public CustomersPage() {
		PageFactory.initElements(driver, this);
	}
	
	public AddCustomerPage navigateAddCustomerPage() {
		addCustomer.click();
		
		return new AddCustomerPage();
	}

	public String getSuccessMsg() {
		
		new WebDriverWait(driver, 15).until(ExpectedConditions.visibilityOf(successBanner));
		return successBanner.getText();
	}
}

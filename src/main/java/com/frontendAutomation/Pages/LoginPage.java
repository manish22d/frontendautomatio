package com.frontendAutomation.Pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.frontendAutomation.BaseClass.TestBase;

public class LoginPage  extends TestBase{
	
	@FindBy(name="username")
	WebElement username;
	
	@FindBy(name="password")
	WebElement password;
	
	@FindBy(xpath="//button[@class='_2AkmmA _1LctnI _7UHT_c' and @type='submit']") 
	WebElement loginButton;
	
	@FindBy(xpath="//button[contains(text(),'Sign Up')]")
	WebElement signUpButton;
	
	@FindBy(xpath="//img[contains(@class,'img-responsive')]")
	WebElement crmLogo;
	
	//div[@class='Km0IJL col col-3-5']//input
	
	@FindBy(xpath="//div[@class='Km0IJL col col-3-5']//input")
	WebElement ModalUsername;
	
	
	@FindBy(xpath="//div[@class='Km0IJL col col-3-5']//input")
	WebElement ModalPassword;
	
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	public String validateLoginPageTitle()
	{
		return driver.getTitle();
	}
	
	public boolean validateCRMImage()
	{
		return crmLogo.isDisplayed();
	}
	
	public HomePage login(String uname, String pword)
	{
		
		System.out.println(uname+" "+pword);
		ModalUsername.sendKeys(uname);
		ModalUsername.sendKeys(Keys.TAB);
		ModalUsername.sendKeys(Keys.TAB,pword);
		ModalPassword.sendKeys(pword);
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("arguments[0].click();", loginButton);
				
		return new HomePage();
	}

}

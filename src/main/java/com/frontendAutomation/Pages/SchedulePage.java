package com.frontendAutomation.Pages;

import static org.hamcrest.MatcherAssert.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.frontendAutomation.BaseClass.TestBase;

public class SchedulePage extends TestBase {

	@FindBy(xpath = "//*[@id='cal']//button")
	WebElement openClass;

	@FindBy(xpath = "//a[contains(text(),'Edit Class')]")
	WebElement editClass;

	@FindBy(xpath = "//a[contains(text(),'Delete Class')]")
	WebElement deleteClass;

	@FindBy(id = "calendar-event")
	WebElement classDetails;

	@FindBy(xpath = "//*[@id='event-date']/preceding-sibling::a")
	WebElement classDetailsCloseButton;

	@FindBy(className = "event-title")
	List<WebElement> classes;

	@FindBy(name = "customer_select")
	WebElement searchCustomerDropDown;

	@FindBy(xpath = "//button[contains(text(),'Add Customer to Class')]")
	WebElement addCustomerButton;

	@FindBy(xpath = "//*[@id='formpackage']//button[contains(text(),'Add without a Package')]")
	WebElement addWithoutPackageButton;

	@FindBy(xpath = "//*[@id='addPackageHref']")
	WebElement addPackageButton;

	@FindBy(id = "package_select")
	WebElement selectPackageDropdown;

	@FindBy(id = "payment_method")
	WebElement selectPaymentDropdown;

	@FindBy(xpath = "//*[@id='btnAddPackage']/button")
	WebElement addPackageToCustomer;

	@FindBy(id = "deleteHref")
	WebElement cancelBookingButton;

	public SchedulePage() {
		PageFactory.initElements(driver, this);
	}

	public int getNumberOfClasses() {
		System.out.println("Total Classes are " + classes.size());
		return classes.size();
	}

	// Issue with last class
	public void verifyButtonDisplayedInClass() throws InterruptedException {
		System.out.println(classes);
		for (int i = 0; i < classes.size() - 2; i++) {
			Thread.sleep(2000);
			System.out.println("this is iteration -> " + i);
			classes.get(i).click();
			System.out.println("Testing event-> " + classes.get(i).getText());
			new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(openClass));
			assertThat("Class details not displayed", classDetails.isDisplayed());
			assertThat("Open Button not displayed", openClass.isDisplayed());
			assertThat("Edit Button not displayed", editClass.isDisplayed());
			assertThat("Delete button not displayed", deleteClass.isDisplayed());
			Thread.sleep(2000);
			classDetailsCloseButton.click();
		}
		;
	}

	public void selectClassAfterCurrentDate() throws InterruptedException {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZZZ");
		String currentDate = dateFormat.format(new Date());

		driver.findElement(By.xpath("//a[@data-date='31']")).click();
		Thread.sleep(10000);
		List<WebElement> classes = driver
				.findElements(By.xpath("//div[@class='event-container bg-success-lighter readonly']"));

		for (int i = 0; i < classes.size() - 1; i++) {
			WebElement ele = classes.get(i);

			new WebDriverWait(driver, 20).until(ExpectedConditions.elementToBeClickable(ele));
			try {
				if (dateFormat.parse(ele.getAttribute("data-starttime")).after(dateFormat.parse(currentDate))
						&& !ele.getText().equals("YOGA BOOTCAMP")) {
					System.out.println("Clicking on -> " + currentDate);
					ele.click();
					if (!classDetails.isDisplayed())
						ele.click();
					return;
				}
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}

	}

	public void clickOnEditClass() {
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(editClass));
		editClass.click();
	}

	public void clickOnOpenClass() throws InterruptedException {
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(openClass));
		Thread.sleep(5000);
		System.out.println(openClass.getText());
		openClass.click();
	}

	public void selectCustomer(String customerDetails) {
		System.out.println("Trying to select -> " + customerDetails);
		Select customer = new Select(searchCustomerDropDown);
		customer.selectByVisibleText(customerDetails);
		addCustomerButton.click();
	}

	public boolean verifyCustomerGotAddedInList(String customerName) throws InterruptedException {
		Thread.sleep(5000);
		String textToCompare;
		List<WebElement> customers = driver.findElements(By.xpath("//table[@id='table-transactions']/tbody/tr"));
		System.out.println("total row is -> " + customers.size());
		for (int i = 0; i < customers.size(); i++) {
			String fname = driver.findElement(By.xpath("//*[@id='table-transactions']/tbody/tr[" + i + 1 + "]/td[1]/a"))
					.getText();
			String lname = driver.findElement(By.xpath("//*[@id='table-transactions']/tbody/tr[" + i + 1 + "]/td[2]/a"))
					.getText();
			String email = driver.findElement(By.xpath("//*[@id='table-transactions']/tbody/tr[" + i + 1 + "]/td[7]"))
					.getText();

			if (!email.isEmpty()) {
				textToCompare = fname + " " + lname + " (" + email + ")";
			} else {
				textToCompare = fname + " " + lname;
			}
			System.out.println("Text To Compare -> " + textToCompare);
			System.out.println("Customer Name -> " + customerName);
			System.out.println(customerName.contains(textToCompare));
			if (customerName.contains(textToCompare)) {
				customers.get(i).findElement(By.xpath("//button[@type='button' and contains(text(), 'Action')]"))
						.click();
				customers.get(i).findElement(By.id("deleteAction")).click();
				new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(cancelBookingButton));
				cancelBookingButton.click();
				return true;
			}
		}
		return false;
	}

	public void addWithPackage(String customerName) throws InterruptedException {
		selectCustomer(customerName);
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(addPackageButton));
		addPackageButton.click();

		new Select(selectPackageDropdown).selectByVisibleText("1 Week Trial x 2 passes ($100)");
		new Select(selectPaymentDropdown).selectByVisibleText("Cash");

		addPackageToCustomer.click();
		Thread.sleep(5000);
		selectCustomer(customerName);
	}

	public void addWithoutPackage(String customerName) throws InterruptedException {
		selectCustomer(customerName);
		new WebDriverWait(driver, 15).until(ExpectedConditions.elementToBeClickable(addWithoutPackageButton));
		addWithoutPackageButton.click();
		Thread.sleep(5000);
	}
}
